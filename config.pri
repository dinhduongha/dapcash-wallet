!defined(BRAND,var){
#  Default brand
    BRAND_BASE = DapCash
    BRAND_BASE_LO = dapcash
    BRAND = DapCash-Wallet
    BRAND_LO = dapcash-wallet
}

VER_MAJ = 2
VER_MIN = 1
VER_PAT = 6

DEFINES += DAP_BRAND=\\\"$$BRAND\\\"
DEFINES += DAP_BRAND_BASE_LO=\\\"$$BRAND_BASE_LO\\\"
DEFINES += DAP_BRAND_LO=\\\"$$BRAND_LO\\\"
DEFINES += DAP_VERSION=\\\"$$VERSION\\\"

win32 {
    VERSION = $${VER_MAJ}.$${VER_MIN}.$$VER_PAT
    DEFINES += CLI_PATH=\\\"$${BRAND_BASE_LO}-node-cli.exe\\\"
    DEFINES += TOOLS_PATH=\\\"$${BRAND_BASE_LO}-node-tool.exe\\\"
    DEFINES += HAVE_STRNDUP
}
else {
    VERSION = $$VER_MAJ\.$$VER_MIN\-$$VER_PAT
    DEFINES += CLI_PATH=\\\"/opt/$${BRAND_BASE_LO}-node/bin/$${BRAND_BASE_LO}-node-cli\\\"
    DEFINES += TOOLS_PATH=\\\"/opt/$${BRAND_BASE_LO}-node/bin/dapcash-node-tool\\\"
    DEFINES += CMD_HISTORY=\\\"/opt/$${BRAND_BASE_LO}/var/log/cmd_history.txt\\\"
    DEFINES += DAP_PATH_PREFIX=\\\"/opt/$${BRAND_BASE_LO}\\\"
    DEFINES += LOG_FILE=\\\"/opt/$${BRAND_BASE_LO}-node/var/log/dapcash-node.log\\\"
}
