QT += core network

CONFIG += c++11 console
CONFIG -= app_bundle
include(../config.pri)

LIBS += -ldl
#LIBS+=-lz #-lz -lrt -lm -lpthread   -lrt -lm -lpthread
#+LIBS+=-lrt
TARGET = $${DAP_BRAND}Service

win32 {
    CONFIG -= console
    HEADERS += $$PWD/../windows/service/Service.h
    SOURCES += $$PWD/../windows/service/Service.cpp
    INCLUDEPATH += $$PWD/../windows/service/
}

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    $$PWD/DapServiceController.cpp \
    $$PWD/main.cpp

HEADERS += \
    $$PWD/DapServiceController.h

#include (../dap-ui-sdk/qml/libdap-qt-ui-qml.pri)
include (../dap-ui-sdk/core/libdap-qt.pri)
include (../dapcash-node/cellframe-sdk/dap-sdk/core/libdap.pri)
include (../dapcash-node/cellframe-sdk/dap-sdk/crypto/libdap-crypto.pri)
include (../dapcash-node/cellframe-sdk/dap-sdk/net/libdap-net.pri)
include (../cellframe-ui-sdk/chain/wallet/libdap-qt-chain-wallet.pri)
#include (../cellframe-ui-sdk/ui/chain/wallet/libdap-qt-ui-chain-wallet.pri)

INCLUDEPATH += $$_PRO_FILE_PWD_/../dapcash-node/
               $$_PRO_FILE_PWD_/../dapRPCProtocol/

unix: !mac : !android {
    service_target.files = $${DAP_BRAND}Service
    service_target.path = /opt/$${DAP_BRAND}/bin/
    INSTALLS += service_target
    BUILD_FLAG = static
}

defined(BUILD_FLAG,var){
    LIBS += -L/usr/lib/icu-static -licuuc -licui18n -licudata
}

RESOURCES += \
    $$PWD/DapCashWalletService.qrc

DISTFILES += \
    classdiagram.qmodel
