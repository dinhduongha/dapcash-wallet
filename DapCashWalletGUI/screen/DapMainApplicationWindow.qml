import QtQuick 2.4
import "qrc:/screen"
import "qrc:/resources/QML"
import "qrc:/screen/desktop"
import "qrc:/screen/desktop/Certificates"
import "qrc:/screen/desktop/NetworksPanel"

Item {
    id: dapMainWindow

    readonly property string walletScreen: "qrc:/screen/" + device + "/Wallet/DapWalletTab.qml"
    readonly property string exchangeScreen: "qrc:/screen/" + device + "/Exchange/DapExchangeTab.qml"
    readonly property string historyScreen: "qrc:/screen/" + device + "/History/DapHistoryTab.qml"
    readonly property string vpnServiceScreen: "qrc:/screen/" + device + "/VPNService/DapVPNServiceTab.qml"
    readonly property string settingsScreen: "qrc:/screen/" + device + "/Settings/DapSettingsTab.qml"
    readonly property string logsScreen: "qrc:/screen/" + device + "/Logs/DapLogsTab.qml"
    readonly property string consoleScreen: "qrc:/screen/" + device + "/Console/DapConsoleTab.qml"
    readonly property string certificatesScreen: "qrc:/screen/" + device + "/Certificates/DapCertificatesMainPage.qml"
    readonly property string underConstructions: "qrc:/screen/" + device + "/DapUnderConstructions.qml"

    property alias dapQuicksandFonts: quicksandFonts
    DapFontQuicksand {
        id: quicksandFonts
    }

    Row
    {
        id: rowMainWindow
        anchors {
            left: parent.left;
            top: parent.top;
            right: parent.right;
            bottom: networksPanel.top
//            bottomMargin: 4 * pt
        }
        Column
        {
            id: columnMenuTab
            height: rowMainWindow.height
            width: 180 * pt
            // Logotype widget
            Item
            {
                id: logotype
                data: dapLogotype
                width: columnMenuTab.width
                height: 60 * pt
                Rectangle
                {
                    id: frameLogotype
                    anchors.fill: parent
                    color: "#241E46"
                    height: 70 * pt
//                        radius: 8 * pt
                    anchors { left: parent.left; right: parent.right; top: parent.top }
                    Image
                    {
                        id: iconLogotype
                        anchors.verticalCenter: parent.verticalCenter
                        width: 99 * pt
                        height: 30 * pt
                        anchors.horizontalCenter: parent.horizontalCenter
                        source: "qrc:/resources/leftPanel/Logo-DapCash-Wallet.svg"
                    }
                }
            }
            // Menu bar widget
            Item
            {
                id: menuWidget
                data: DapAbstractMenuTabWidget
                {
//                        radius: 8 * pt
                    anchors.leftMargin: -8*pt

                    onPathScreenChanged:
                    {
                        stackViewTabs.setSource(Qt.resolvedUrl(this.pathScreen))
                    }
                    id: menuTabWidget
                    anchors.fill: parent
                    dapFrameMenuTab.width: 180 * pt
                    heightItemMenu: 60 * pt
//                    normalColorItemMenu: "#241E46"
//                    selectColorItemMenu: "#2A2352"
                    normalColorItemMenu: "transparent"
                    selectColorItemMenu: "#f1352b6a"
                    widthIconItemMenu: 18 * pt
                    heightIconItemMenu: 18 * pt
                    dapMenuWidget.model: modelMenuTab
                    normalFont: "Quicksand"
                    selectedFont: "Quicksand"
                }

                width: 200*pt
                height: columnMenuTab.height - logotype.height
            }


        }

        // Screen downloader widget
        Item
        {
            id: screens
            data: dabScreensWidget
            height: rowMainWindow.height
            width: rowMainWindow.width - columnMenuTab.width
            Loader
            {
                id: stackViewTabs
                anchors.fill: parent
                clip: true
                source: walletScreen
            }
        }
    }

    DapNetworksPanel
    {
        id: networksPanel
        y: parent.height - height
        width: parent.width
    }

    DapNetworkPopup
    {
        id: networkPanelPopup
    }

    property var dapWallets: []
    signal modelWalletsUpdated()

    Component{
        DapCertificatesMainPage { }
    }
    ListModel
    {
        id: dapNetworkModel
    }

    ListModel
    {
        id: dapModelWallets
    }
    ListModel {
        id: modelMenuTab

        Component.onCompleted:
        {
            append({
                name: qsTr("Wallet"),
                page: walletScreen,
                normalIcon: "qrc:/resources/icons/new_icons/icon_wallet.svg",
                hoverIcon: "qrc:/resources/icons/new_icons/icon_wallet.svg"
            })
            append({
                name: qsTr("Stock"),
                page: underConstructions,
                normalIcon: "qrc:/resources/icons/new_icons/icon_exchange.svg",
                hoverIcon: "qrc:/resources/icons/new_icons/icon_exchange.svg"
            })
            append({
                name: qsTr("History"),
                page: underConstructions,
                normalIcon: "qrc:/resources/icons/new_icons/icon_history.svg",
                hoverIcon: "qrc:/resources/icons/new_icons/icon_history.svg"
            })
            append({
                name: qsTr("Certificates"),
                page: certificatesScreen,
                normalIcon: "qrc:/resources/leftPanel/icon_certificates.svg",
                hoverIcon: "qrc:/resources/leftPanel/icon_certificates.svg"
            })
            append({
                name: qsTr("VPN Client"),
                page: underConstructions,
                normalIcon: "qrc:/resources/icons/new_icons/vpn-client_icon.svg",
                hoverIcon: "qrc:/resources/icons/new_icons/vpn-client_icon.svg"
            })
            append({
                name: qsTr("VPN Service"),
                page: underConstructions,
                normalIcon: "qrc:/resources/icons/new_icons/icon_vpn.svg",
                hoverIcon: "qrc:/resources/icons/new_icons/icon_vpn.svg"
            })
            append({
                name: qsTr("Console"),
                page: consoleScreen,
                normalIcon: "qrc:/resources/icons/new_icons/icon_console.svg",
                hoverIcon: "qrc:/resources/icons/new_icons/icon_console.svg"
            })
//            append({
//                name: qsTr("Logs"),
//                page: logsScreen,
//                normalIcon: "qrc:/resources/icons/new_icons/icon_logs.svg",
//                hoverIcon: "qrc:/resources/icons/new_icons/icon_logs.svg"
//            })
            append({
                name: qsTr("Settings"),
                page: underConstructions,
                normalIcon: "qrc:/resources/icons/new_icons/icon_settings.svg",
                hoverIcon: "qrc:/resources/icons/new_icons/icon_settings.svg"
            })
        }
    }
    Component.onCompleted:
    {
        dapServiceController.requestToService("DapGetListNetworksCommand")
    }

    Connections
    {
        target: dapServiceController

        onNetworksListReceived:
        {
            console.log("Networks list received")

            if (!networkList)
                console.error("networkList is empty")
            else
            {
                dapServiceController.CurrentNetwork = networkList[0];
                dapServiceController.IndexCurrentNetwork = 0;

                console.log("Current network: "+dapServiceController.CurrentNetwork)
            }

            for(var n=0; n < Object.keys(networkList).length; ++n)
            {
                dapNetworkModel.append({name: networkList[n]})
            }
        }

        onWalletsReceived:
        {
            console.log(walletList.length)
            console.log(dapWallets.length)
            console.log(dapModelWallets.count)
            for (var q = 0; q < walletList.length; ++q)
            {
                dapWallets.push(walletList[q])
            }
            for (var i = 0; i < dapWallets.length; ++i)
            {
                console.log("Wallet name: "+ dapWallets[i].Name)
                dapModelWallets.append({ "name" : dapWallets[i].Name,
                                      "balance" : dapWallets[i].Balance,
                                      "icon" : dapWallets[i].Icon,
                                      "address" : dapWallets[i].Address,
                                      "networks" : []})
                console.log("Networks number: "+Object.keys(dapWallets[i].Networks).length)
                for (var n = 0; n < Object.keys(dapWallets[i].Networks).length; ++n)
                {
                    console.log("Network name: "+dapWallets[i].Networks[n])
                    print(dapModelWallets.get(i).networks)
                    print("name", dapWallets[i].Networks[n])
                    print("address", dapWallets[i].findAddress(dapWallets[i].Networks[n]))
                    dapModelWallets.get(i).networks.append({"name": dapWallets[i].Networks[n],
                          "address": dapWallets[i].findAddress(dapWallets[i].Networks[n]),
                          "tokens": []})
                    console.log(Object.keys(dapWallets[i].Tokens).length)
                    for (var t = 0; t < Object.keys(dapWallets[i].Tokens).length; ++t)
                    {
                        console.log(dapWallets[i].Tokens[t].Network + " === " + dapWallets[i].Networks[n])
                        if(dapWallets[i].Tokens[t].Network === dapWallets[i].Networks[n])
                        {
                             dapModelWallets.get(i).networks.get(n).tokens.append(
                                 {"name": dapWallets[i].Tokens[t].Name,
                                  "balance": dapWallets[i].Tokens[t].Balance,
                                  "emission": dapWallets[i].Tokens[t].Emission,
                                  "network": dapWallets[i].Tokens[t].Network})
                        }
                    }
                }
            }
            modelWalletsUpdated();
        }
    }
}
