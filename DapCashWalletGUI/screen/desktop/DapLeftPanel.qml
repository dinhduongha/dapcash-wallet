import QtQuick 2.4
import "qrc:/screen"

Rectangle {
    id: root

    property string currentTabName

    readonly property string walletTabName      : "Wallet"
    readonly property string stockTabName       : "Stock"
    readonly property string historyTabName     : "History"
    readonly property string certificatesTabName: "Certificates"
    readonly property string tokensTabName      : "Tokens"
    readonly property string vpnClientTabName   : "VPN client"
    readonly property string vpnServiceTabName  : "VPN service"
    readonly property string consoleTabName     : "Console"
    readonly property string logsTabName        : "Logs"
    readonly property string settingsTabName    : "Settings"

    color: "#241E46"
//    radius: 8 * pt

    Rectangle {
        id: radiusHideRect

        color: root.color
        width: root.radius
        height: root.height
        x:0; y:0
    }

    Item {
        id: logoItem

        height: 70 * pt
        anchors { left: parent.left; right: parent.right; top: parent.top }

        Image {
            x: 25 * pt
            y: 17 * pt
            id: logoImage
            sourceSize { width:99; height: 30 }
            source: "qrc:/resources/leftPanel/Logo-DapCash-Wallet.svg"
        }
    }
    Item{
        id:menuWidget
        data: DapAbstractMenuTabWidget
    }

    ListView {
        id: tabsView

        property double scaleKoef: 1.1

        anchors { top: logoItem.bottom; left:  parent.left; right: parent.right; bottom: parent.bottom }
        interactive: false
        model: tabsModel

        delegate: Item {
            id: itemDelegate

            property bool isActive: tabName == root.currentTabName
            height: 50 * pt
            width: root.width
            opacity: tabEnabled ? 1 : 0.5

            MouseArea {
                anchors.fill: parent
                enabled: !itemDelegate.isActive && tabEnabled
                hoverEnabled: true
                onEntered: PropertyAnimation { target: scalableItem; property: "curentScale"; duration: 100; to: tabsView.scaleKoef }
                onExited:  PropertyAnimation { target: scalableItem; property: "curentScale"; duration: 100; to: 1 }
            }

            Rectangle {
                color: "#FFFFFF"
                radius: 8 * pt
                anchors {
                    left: parent.left
                    leftMargin: -radius
                    top: parent.top
                    bottom: parent.bottom
                    right: parent.right
                    rightMargin: 10 * pt
                }

                visible: itemDelegate.isActive
            }

            Item {
                id: scalableItem

                property double curentScale: 1

                anchors.fill: parent
                transform: Scale {
                    id: scale
                    origin.x: itemIcon.x + itemIcon.width/2
                    origin.y: itemIcon.y + itemIcon.height/2
                    xScale: scalableItem.curentScale
                    yScale: scalableItem.curentScale

                }

                Image {
                    id: itemIcon

                    source: itemDelegate.isActive ? activeIcon : icon
                    x: 24 * pt
                    y: 17 * pt
                    width: 18 * pt
                    height: 18 * pt
                    sourceSize.height: 18 * pt * scalableItem.curentScale
                    sourceSize.width: 18 * pt * scalableItem.curentScale
                }

                Text {
                    x: 54 * pt
                    y: 16 * pt
                    text: tabName
                    color: itemDelegate.isActive ? "#2A2352" :  "#FFFFFF"
                    font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular16
                }
            }


        }
    }

    ListModel {
        id: tabsModel

        Component.onCompleted:
        {
            append({
                name: qsTr("Wallet"),
                page: walletScreen,
                normalIcon: "qrc:/resources/leftPanel/icon_wallet.svg",
                hoverIcon: "qrc:/resources/leftPanel/icon_wallet_active.svg"
            })
            append({
                name: qsTr("Stock"),
                page: underConstructions,
                normalIcon: "qrc:/resources/leftPanel/icon_exchange.svg",
                hoverIcon: "qrc:/resources/leftPanel/icon_exchange_active.svg"
            })
            append({
                name: qsTr("History"),
                page: historyScreen,
                normalIcon: "qrc:/resources/leftPanel/icon_history.svg",
                hoverIcon: "qrc:/resources/leftPanel/icon_history_active.svg"
            })
            append({
                name: qsTr("Certificates"),
                page: certificatesScreen,
                normalIcon: "qrc:/resources/leftPanel/icon_certificates.svg",
                hoverIcon: "qrc:/resources/leftPanel/icon_certificates_active.svg"
            })
            append({
                name: qsTr("VPN Client"),
                page: underConstructions,
                normalIcon: "qrc:/resources/leftPanel/icon_vpnclient.svg",
                hoverIcon: "qrc:/resources/leftPanel/icon_vpnclient_active.svg"
            })
            append({
                name: qsTr("VPN Service"),
                page: vpnServiceScreen,
                normalIcon: "qrc:/resources/leftPanel/icon_vpnservice.svg",
                hoverIcon: "qrc:/resources/leftPanel/icon_vpnservice_active.svg"
            })
            append({
                name: qsTr("Console"),
                page: consoleScreen,
                normalIcon: "qrc:/resources/leftPanel/icon_console.svg",
                hoverIcon: "qrc:/resources/leftPanel/icon_console_active.svg"
            })
            append({
                name: qsTr("Logs"),
                page: logsScreen,
                normalIcon: "qrc:/resources/leftPanel/icon_logs.svg",
                hoverIcon: "qrc:/resources/leftPanel/icon_logs_active.svg"
            })
            append({
                name: qsTr("Settings"),
                page: settingsScreen,
                normalIcon: "qrc:/resources/leftPanel/icon_settings.svg",
                hoverIcon: "qrc:/resources/leftPanel/icon_settings_active.svg"
            })
        }
    }
}
