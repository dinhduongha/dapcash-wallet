import QtQuick 2.7
import QtQuick.Controls 2.2

RadioButton {
    id: control

    font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular16

    indicator: Rectangle {
        implicitWidth: 17 * pt
        implicitHeight: implicitWidth
        x: control.leftPadding
        anchors.verticalCenter: contentItem.verticalCenter
        color: "#00000000"
        border { width: 2 * pt; color: "#2A2352" }
        radius: width * 0.5

        Rectangle {
            implicitWidth: 9 * pt
            implicitHeight: implicitWidth
            x: (parent.width - width) * 0.5
            y: (parent.height - height) * 0.5
            visible: control.checked
            color: "#2A2352"
            radius: width * 0.5
        }
    }

    contentItem: Text {
        text: control.text
        font: control.font
        color: "#2A2352"
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
        leftPadding: control.indicator.width + control.spacing
    }
}
