import QtQuick 2.7
import QtQuick.Controls 2.2

Button {
    id: control

    property color textColor: "#FFFFFF"
    property color textColorHover: "#FFFFFF"
    property color textColorDisabled: "#FFFFFF"
    property color textColorChecked: "#FFFFFF"

    property color backgroundColor: "#443D6A"
    property color backgroundColorHover: "#9A88FF"
    property color backgroundColorDisabled: "#E2E1E6"
    property color backgroundColorChecked: "#9A88FF"

    implicitHeight: 36 * pt
    leftPadding: 0
    topPadding: 0
    rightPadding: 0
    bottomPadding: 0
    font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular16

    contentItem: Text {
        font: control.font
        elide: Text.ElideRight
        color: control.enabled ? (control.checked ? control.textColorChecked : (control.hovered ? control.textColorHover : control.textColor)) : control.textColorDisabled
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        text: control.text
    }

    background: Rectangle {
        radius: 4 * pt
        color: control.enabled ? (control.checked ? control.backgroundColorChecked : (control.hovered ? control.backgroundColorHover : control.backgroundColor)) : control.backgroundColorDisabled
    }
}
