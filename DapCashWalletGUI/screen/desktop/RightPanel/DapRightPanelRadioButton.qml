import QtQuick 2.7
import "../Controls"

Item {
    id: control

    property alias radioButton: _radioButton

    property real leftInset: 36 * pt
    property real rightInset: 36 * pt

    implicitWidth: _radioButton.implicitWidth + leftInset + rightInset
    implicitHeight: 52 * pt

    DapRadioButton {
        id: _radioButton

        x: control.leftInset
        anchors.verticalCenter: control.verticalCenter
        width: control.width - control.leftInset - control.rightInset
        height: Math.min(implicitHeight, control.height)
        padding: 0
    }
}
