import QtQuick 2.7
import QtQuick.Controls 2.2

Item {
    id: control

    property alias checkBox: _checkBox

    property real leftInset: 36 * pt
    property real rightInset: 36 * pt

    implicitWidth: _checkBox.implicitWidth + leftInset + rightInset
    implicitHeight: 40 * pt

    CheckBox {
        id: _checkBox

        x: control.leftInset
        anchors.verticalCenter: control.verticalCenter
        width: control.width - control.leftInset - control.rightInset
        height: Math.min(implicitHeight, control.height)

        padding: 0
        font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular16

        indicator: Image {
            anchors.verticalCenter: _checkBox.contentItem.verticalCenter
            sourceSize: Qt.size(20 * pt, 20 * pt)
            source: _checkBox.checked ? "qrc:/resources/icons/ic_checkbox_active.svg" : "qrc:/resources/icons/ic_checkbox.svg"
        }

        contentItem: Text {
            text: _checkBox.text
            font: _checkBox.font
            color: "#2A2352"
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
            leftPadding: _checkBox.indicator.width + _checkBox.spacing
        }

    }

}
