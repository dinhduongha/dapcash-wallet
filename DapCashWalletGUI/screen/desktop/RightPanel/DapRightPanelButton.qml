import QtQuick 2.7
import "../Controls"

Item {
    property alias button: btn

    implicitWidth: btn.implicitWidth
    implicitHeight: btn.implicitHeight + 20 * pt

    DapTextButton {
        id: btn

        anchors.centerIn: parent
        width: Math.min(parent.width, implicitWidth)
        height: Math.min(parent.height, implicitHeight)
    }
}
