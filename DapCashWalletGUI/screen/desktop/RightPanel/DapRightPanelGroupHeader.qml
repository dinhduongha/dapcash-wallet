import QtQuick 2.7

Item {
    id: control

    property alias text: textName.text

    property real topInset: 0
    property real bottomInset: 0

    implicitWidth: textName.implicitWidth
    implicitHeight: Math.max(background.implicitHeight, textName.implicitHeight) + topInset + bottomInset

    Rectangle {
        id: background

        implicitHeight: 30 * pt

        y: control.topInset
        width: parent.width
        height: parent.height - control.topInset - control.bottomInset

        color: "#49436A"

        Text {
            id: textName

            x: 15 * pt
            anchors.verticalCenter: parent.verticalCenter
            width: Math.min(implicitWidth, parent.width)
            height: Math.min(implicitHeight, parent.height)
            font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular12
            elide: Text.ElideRight
            color: "#FFFFFF"
        }
    }
}
