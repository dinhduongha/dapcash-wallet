import QtQuick 2.7
import QtQuick.Controls 2.2

Item {
    id: control

    property alias textField: _textField

    property real leftInset: 36 * pt
    property real rightInset: 36 * pt

    implicitWidth: _textField.implicitWidth + leftInset + rightInset
    implicitHeight: 40 * pt

    TextField {
        id: _textField

        x: control.leftInset
        anchors.verticalCenter: control.verticalCenter
        width: control.width - control.leftInset - control.rightInset
        height: Math.min(implicitHeight, control.height)

        padding: 0
        font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular16
        color: "#2A2352"
        selectByMouse: true

        background: null
    }
}
