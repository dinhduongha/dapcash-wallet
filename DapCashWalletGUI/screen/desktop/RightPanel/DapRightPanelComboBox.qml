import QtQuick 2.7
import "qrc:/widgets"

Item {
    id: control

    property alias comboBox: _comboBox
    property alias currentText: _comboBox.currentText

    implicitWidth: 320*pt
    implicitHeight: 50 * pt

    DapComboBox {
        id: _comboBox
        anchors.centerIn: parent
        indicatorImageNormal: "qrc:/resources/icons/ic_arrow_drop_down_dark.png"
        indicatorImageActive: "qrc:/resources/icons/ic_arrow_drop_up.png"
        sidePaddingNormal: 0 * pt
        sidePaddingActive: 20 * pt
        normalColorText: "#2A2352"
        hilightColorText: "#FFFFFF"
        normalColorTopText: "#2A2352"
        hilightColorTopText: "#070023"
        hilightColor: "#9A88FF"
        normalTopColor: "transparent"
        widthPopupComboBoxNormal: 320*pt
        widthPopupComboBoxActive: widthPopupComboBoxNormal + 40 * pt
        heightComboBoxNormal: 24 * pt
        heightComboBoxActive: 44 * pt
        bottomIntervalListElement: 0 * pt
        topEffect: false
        normalColor: "#FFFFFF"
        hilightTopColor: normalColor
        paddingTopItemDelegate: 10 * pt
        heightListElement: 42 * pt
        intervalListElement: 0 * pt
        indicatorWidth: 20 * pt
        indicatorHeight: indicatorWidth
        indicatorLeftInterval: 10 * pt
        colorTopNormalDropShadow: "#00000000"
        colorDropShadow: "#40ABABAB"
        fontComboBox: [dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular16]
        colorMainTextComboBox: [["#2A2352", "#2A2352"]]
        colorTextComboBox: [["#2A2352", "#FFFFFF"]]
    }
}
