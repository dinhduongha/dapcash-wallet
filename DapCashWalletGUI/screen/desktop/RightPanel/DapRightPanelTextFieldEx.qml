import QtQuick 2.7
import QtQuick.Controls 2.2

// Text field with bottom line and clear button
Item {
    id: control

    property alias text: _textField.text
    property alias textField: _textField

    property real leftInset: 36 * pt
    property real rightInset: 36 * pt

    implicitWidth: _textField.implicitWidth + leftInset + rightInset
    implicitHeight: 50 * pt

    TextField {
        id: _textField

        x: control.leftInset
        anchors.verticalCenter: control.verticalCenter
        width: control.width - control.leftInset - control.rightInset
        height: Math.min(implicitHeight, control.height)

        leftPadding: 0
        rightPadding: btnClear.width
        topPadding: 3 * pt
        bottomPadding: 4 * pt // for space between line and text

        font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular16
        color: "#2A2352"
        selectByMouse: true

        background: Item {
            Item {
                x: _textField.leftPadding
                y: _textField.topPadding
                width: _textField.width
                height: _textField.height

                Rectangle {
                    y: parent.height - height
                    width: parent.width
                    height: 1 * pt
                    color: "#B4B1BD"
                }

                Image {
                    id: btnClear

                    anchors.right: parent.right
                    y: (parent.height - _textField.bottomPadding - height) * 0.5
                    visible: _textField.length > 0
                    sourceSize: Qt.size(20 * pt, 20 * pt)
                    source: btnClearMouseArea.containsMouse ? "qrc:/resources/icons/icon_delete_hover.svg" : "qrc:/resources/icons/icon_delete.svg"

                    MouseArea {
                        id: btnClearMouseArea

                        width: parent.width
                        height: parent.height
                        hoverEnabled: true

                        onClicked: _textField.clear()
                    }
                }
            }
        }
    }
}
