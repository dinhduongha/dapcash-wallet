import QtQuick 2.7
import "../Controls"
import "qrc:/widgets"

Item {
    id: control

    property alias spinBox: _spinBox
    property alias comboBox: _comboBox

    property alias amount: _spinBox.value
    property alias amountString: _spinBox.text
    property int datoshiAmount: amount * 1000000000
    property string token: _comboBox.currentText

    property real leftInset: 36 * pt
    property real rightInset: 36 * pt

    implicitWidth: leftInset + rightInset + _spinBox.implicitWidth + _comboBox.widthPopupComboBoxNormal
    implicitHeight: 50 * pt

    Item {
        x: control.leftInset
        width: control.width - control.leftInset - control.rightInset
        height: control.height

        DapDoubleSpinBox {
            id: _spinBox

            anchors.verticalCenter: parent.verticalCenter
            width: parent.width / 2

            from: 0
            to: 9999999999
            decimals: 9//_comboBox.currentIndex >= 0 && _comboBox.currentIndex < _comboBox.count ? _comboBox.model.get(_comboBox.currentIndex).decimals : 2
        }

        Item {
            x: Math.floor(parent.width * 0.65)
            width: parent.width - x
            height: parent.height

            DapComboBox {
                id: _comboBox

                anchors.centerIn: parent
                indicatorImageNormal: "qrc:/resources/icons/ic_arrow_drop_down_dark.png"
                indicatorImageActive: "qrc:/resources/icons/ic_arrow_drop_up.png"
                sidePaddingNormal: 0 * pt
                sidePaddingActive: 20 * pt
                normalColorText: "#2A2352"
                hilightColorText: "#FFFFFF"
                normalColorTopText: "#2A2352"
                hilightColorTopText: "#070023"
                hilightColor: "#6455B8"
                normalTopColor: "transparent"
                widthPopupComboBoxNormal:  parent.width
                widthPopupComboBoxActive: widthPopupComboBoxNormal + 40 * pt
                heightComboBoxNormal: 24 * pt
                heightComboBoxActive: 44 * pt
                bottomIntervalListElement: 8 * pt
                topEffect: false
                normalColor: "#FFFFFF"
                hilightTopColor: normalColor
                paddingTopItemDelegate: 8 * pt
                heightListElement: 32 * pt
                intervalListElement: 10 * pt
                indicatorWidth: 20 * pt
                indicatorHeight: indicatorWidth
                indicatorLeftInterval: 10 * pt
                colorTopNormalDropShadow: "#00000000"
                colorDropShadow: "#40ABABAB"
                fontComboBox: [dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular18]
                colorMainTextComboBox: [["#2A2352", "#2A2352"]]
                colorTextComboBox: [["#2A2352", "#FFFFFF"]]

                // TODO
                comboBoxTextRole: ["token"]
                model: ListModel {
                }
            }
        }
    }
}
