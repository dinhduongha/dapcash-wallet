import QtQuick 2.7
import "../Controls"

Item {
    property alias button1: btn1
    property alias button2: btn2

    implicitWidth: btn1.implicitWidth + btn2.implicitWidth
    implicitHeight: Math.max(btn1.implicitHeight, btn2.implicitHeight) + 20 * pt

    DapTextButton {
        id: btn1

        x: (parent.width / 2 - width) / 2
        anchors.verticalCenter: parent.verticalCenter
        width: Math.min(parent.width / 2, implicitWidth)
        height: Math.min(parent.height, implicitHeight)
    }

    DapTextButton {
        id: btn2

        x: parent.width / 2 + (parent.width / 2 - width) / 2
        anchors.verticalCenter: parent.verticalCenter
        width: Math.min(parent.width / 2, implicitWidth)
        height: Math.min(parent.height, implicitHeight)
    }
}
