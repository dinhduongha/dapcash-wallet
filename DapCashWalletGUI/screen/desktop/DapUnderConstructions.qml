import QtQuick 2.0
import "qrc:/resources/icons/"

Rectangle{
        color: "white"
        anchors.fill: parent
        Rectangle{
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            width: 520
            height: 400


            Image {
                id: under_cunstruct_img
                anchors.fill: parent
                source: "qrc:/resources/icons/under construction.svg"
                anchors.centerIn: parent.Center
                sourceSize.width: parent.width
                sourceSize.height: parent.height
            }
        }
}
