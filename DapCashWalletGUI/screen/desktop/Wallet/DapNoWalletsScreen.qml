import QtQuick 2.7
import "qrc:/widgets"
import "../Controls"
import "../../"

Item {
    id: control

    Image {
        y: 114 * pt
        anchors.horizontalCenter: parent.horizontalCenter
        sourceSize: Qt.size(500 * pt, 315 * pt)
        source: "qrc:/resources/illustrations/illustration_wallet.png"
    }

    Text {
        y: 478 * pt
        anchors.horizontalCenter: parent.horizontalCenter
        font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandMedium26 // TODO set size 24
        color: "#2A2352"
        text: qsTr("To add or create a wallet click on the button below")
    }

    DapTextButton {
        y: 571 * pt
        anchors.horizontalCenter: parent.horizontalCenter
        width: 180 * pt
        text: qsTr("New wallet")

        onClicked:{
            app.stateMachine.submitEvent(app.stateMachine.walletCreatingStarted);
        }
    }
}
