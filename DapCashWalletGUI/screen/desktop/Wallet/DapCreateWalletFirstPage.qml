import QtQuick 2.7
import QtQuick.Controls 2.2
import "../RightPanel"


DapRightPanelPage {
    id: page

    // for scroll support
    implicitHeight: column.height
    property string dapSignatureTypeWallet

    caption: qsTr("New wallet")

    Column {
        id: column

        spacing: 0

        DapRightPanelGroupHeader {
            width: page.width
            bottomInset: 10 * pt
            text: qsTr("Name of wallet")
        }
        DapRightPanelTextField {
            id: textName

            width: page.width
            textField.placeholderText: qsTr("Input name of wallet")
        }
        DapRightPanelCheckBox {
            id: checkBoxUseExistingWallet

            width: page.width
            checkBox.text: qsTr("Use existing wallet")
        }

        DapRightPanelGroupHeader {
            width: page.width
            topInset: 10 * pt
            bottomInset: 10 * pt
            text: qsTr("Choose signature type")
        }
        DapRightPanelComboBox {
            id: comboBoxSignatureType

            width: page.width

            comboBox.comboBoxTextRole: ["text"]
            comboBox.model: ListModel {
                ListElement { text: qsTr("Dilithium");signatureText: "sig_dil" }
                ListElement { text: qsTr("Bliss");signatureText: "sig_bliss" }
                ListElement { text: qsTr("Picnic");signatureText: "sig_picnic" }
                ListElement { text: qsTr("Tesla");signatureText: "sig_tesla" }
            }
        }

        DapRightPanelGroupHeader {
            width: page.width
            topInset: 10 * pt
            bottomInset: 16 * pt
            text: qsTr("Recovery method")
            opacity: 0 // TODO
        }

        ButtonGroup {
            id: recoveryMethodButtonGroup
            buttons: [ radioButton24Words.radioButton, radioButtonQRCode.radioButton,
                radioButtonExportToFile.radioButton, radioButtonNothing.radioButton ]
        }
        DapRightPanelRadioButton {
            id: radioButton24Words

            width: page.width
            radioButton.text: qsTr("24 words")
            opacity: 0 // TODO
        }
        DapRightPanelRadioButton {
            id: radioButtonQRCode

            width: page.width
            radioButton.text: qsTr("QR code")
            opacity: 0 // TODO
        }
        DapRightPanelRadioButton {
            id: radioButtonExportToFile

            width: page.width
            radioButton.text: qsTr("Export to file")
            opacity: 0 // TODO
        }
        DapRightPanelRadioButton {
            id: radioButtonNothing

            width: page.width
            radioButton.text: qsTr("Nothing")
            opacity: 0 // TODO
        }

        Item {
            width: 1
            height: 32 * pt
        }

        DapRightPanelButton {
            id: btnNext

            enabled: textName.textField.length > 0 /* TODO && (radioButton24Words.radioButton.checked || radioButtonQRCode.radioButton.checked
                                                       || radioButtonExportToFile.radioButton.checked || radioButtonNothing.radioButton.checked)*/
            width: page.width
            button.width: 132 * pt
            button.text: qsTr("Next")

            button.onClicked:
            {

                dapSignatureTypeWallet = comboBoxSignatureType.comboBox.model.get(comboBoxSignatureType.comboBox.currentIndex).signatureText;
             //   dapServiceController.requestToService("DapAddWalletCommand",textName.textField.text,checkBoxUseExistingWallet.checkBox.checked,comboBoxSignatureType.comboBox.model.get(comboBoxSignatureType.comboBox.currentIndex).signatureText)


                console.log("Create new wallet "+textName.textField.text);
                            console.log("Signature "+dapSignatureTypeWallet);
                            console.log("Network "+dapServiceController.CurrentNetwork)
 // TODO We need to find out why it works on Linux and doesn't work on Windows
//               app.creatWallet(textName.textField.text,checkBoxUseExistingWallet.checkBox.checked,comboBoxSignatureType.comboBox.model.get(comboBoxSignatureType.comboBox.currentIndex).signatureText)
                dapServiceController.requestToService("DapAddWalletCommand", textName.textField.text    //original
                                                                  , dapSignatureTypeWallet, dapServiceController.CurrentNetwork
                                                                  , "0xad12dec5ab4f");
                app.stateMachine.submitEvent(app.stateMachine.walletCreatingRequested)
//                var data = {
//                    walletName   : textName.textField.text,
//                    useExisting  : checkBoxUseExistingWallet.checkBox.checked,
//                    signatureType: comboBoxSignatureType.comboBox.model.get(comboBoxSignatureType.comboBox.currentIndex).signatureText
//                }

            }
        }
    }
}
