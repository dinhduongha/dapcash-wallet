import QtQuick 2.7
import "../RightPanel"

DapRightPanelPage {
    id: page

    property var selectedNetwork: cbbNetwork.comboBox.currentIndex >= 0 && cbbNetwork.comboBox.currentIndex < cbbNetwork.comboBox.count
                                  ? comboBoxNetworksModel.get(cbbNetwork.currentIndex)
                                  : null

    property string network: cbbNetwork.currentText || ''
    property string recieverAddress: textFieldTo.text
    property alias amount: tokenValueField.amount
    property alias amountString: tokenValueField.amountString
    property string token: tokenValueField.token || ''
    property DapBalanceModel networkBalanceModel
    property bool hasTokensAmount: true

    onNetworkChanged: updateHasTokensAmount()
    onTokenChanged: updateHasTokensAmount()
    onAmountChanged: updateHasTokensAmount()
    function updateHasTokensAmount() {
        hasTokensAmount = (networkBalanceModel && token.length > 0) ? networkBalanceModel.hasBalance(token, amountString) : true
    }

    // for scroll support
    implicitHeight: column.height

    caption: qsTr("New payment")

    Component.onCompleted: {
        var networksArray = app.currentWallet.balanceModel.networks;
        for (var i = 0; i < networksArray.length; ++i) {
            comboBoxNetworksModel.append({ name: networksArray[i].name });
        }
        cbbNetwork.comboBox.currentIndex = 0;
    }

    Column {
        id: column

        DapRightPanelGroupHeader {
            width: page.width
            bottomInset: 10 * pt
            text: qsTr("From")
        }

        DapRightPanelComboBox {
            id: cbbNetwork

            comboBox.onCurrentTextChanged: {
                page.networkBalanceModel = app.currentWallet.balanceModel.getBalanceModel(currentText)
                var oldToken = tokenValueField.comboBox.currentText
                tokenValueField.comboBox.model.clear()

                if (!networkBalanceModel)
                    return

                var tokens = networkBalanceModel.tokens

                for (var i = 0; i < tokens.length; ++i) {
                    var currentToken = tokens[i].name
                    tokenValueField.comboBox.model.append({token: currentToken});

                    if (currentToken === oldToken)
                        tokenValueField.comboBox.currentIndex = i
                }
            }

            comboBox.comboBoxTextRole: ["name"]

            anchors { left: parent.left; leftMargin: 35*pt}
            implicitWidth: 280*pt
            comboBox.widthPopupComboBoxNormal:280*pt

            comboBox.model: ListModel {
                id: comboBoxNetworksModel
            }

        }

        DapRightPanelGroupHeader {
            width: page.width
            topInset: 10 * pt
            bottomInset: 10 * pt
            text: qsTr("Amount")
        }

        DapRightPanelTokenValueField {
            id: tokenValueField

            width: page.width
        }

        DapRightPanelGroupHeader {
            width: page.width
            topInset: 10 * pt
            bottomInset: 15 * pt
            text: qsTr("To")
        }

        DapRightPanelTextFieldEx {
            id: textFieldTo

            width: page.width
            textField.onFocusChanged: if (textField.focus && textField.text.length == 0) textField.paste()
            textField.placeholderText: qsTr("Click here to paste")
        }

        Text {
            id: textError

            width: page.width
            leftPadding: 36 * pt
            topPadding: 35 * pt
            rightPadding: 36 * pt
            bottomPadding: 53 * pt
            opacity: !hasTokensAmount ? 1.0 : 0.0
            font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular16
            wrapMode: Text.WordWrap
            horizontalAlignment: Qt.AlignHCenter
            color: "#FF0300"
            text: qsTr("Not enough available tokens.\n Enter a lower value.")
        }

        DapRightPanelButton {
            enabled: page.amount > 0.0 && page.hasTokensAmount
                     && page.network.length > 0 && page.token.length > 0
                     && page.recieverAddress.length > 0

            width: page.width
            button.width: 132 * pt
            button.text: qsTr("Next")

            button.onClicked:
            {
                app.stateMachine.submitEvent(app.stateMachine.paymantCreatingRequested)
                app.creatTransaction(app.currentWallet.Name, page.network, page.recieverAddress, page.amountString, page.token)
            }

        }
    }
}
