import QtQuick 2.7

Item {
    property alias text: _text.text

    Text {
        id: _text

        y: 325 * pt
        anchors.horizontalCenter: parent.horizontalCenter
        width: Math.min(implicitWidth, parent.width)
        font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandMedium26
        elide: Text.ElideRight
        color: "#241E46"
    }

}
