import QtQuick 2.4
import "../../../"

DapDoneWalletRightPanelForm
{
    dapButtonDone.onClicked:
    {
        nextActivated(lastActionsWallet)
        walletTopPanel.dapAddWalletButton.colorBackgroundNormal = "#070023"
    }

    dapButtonClose.onClicked:
    {
        previousActivated(lastActionsWallet)
        walletTopPanel.dapAddWalletButton.colorBackgroundNormal = "#070023"
    }
}
