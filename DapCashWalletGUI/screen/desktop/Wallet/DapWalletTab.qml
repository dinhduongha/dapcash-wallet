import QtQuick 2.4
import QtQuick.Controls 1.4
import "qrc:/"
import "../../"


DapAbstractTab {

    ///@detalis Path to the right panel of transaction history.
//    readonly property string transactionHistoryWallet: "qrc:/screen/" + device + "/Dashboard/RightPanel/DapTransactionHistoryRightPanel.qml"
    ///@detalis Path to the right panel of input name wallet.
    readonly property string inputNameWallet: "qrc:/screen/" + device + "/Wallet/RightPanel/DapInputNewWalletNameRightPanel.qml"
    ///@detalis Path to the right panel of recovery.
//    readonly property string recoveryWallet: "qrc:/screen/" + device + "/Dashboard/RightPanel/DapRecoveryWalletRightPanel.qml"
    ///@detalis Path to the right panel of done.
    readonly property string doneWallet: "qrc:/screen/" + device + "/Wallet/RightPanel/DapDoneWalletRightPanel.qml"
    ///@detalis Path to the right panel of last actions.
    readonly property string lastActionsWallet: "qrc:/screen/" + device + "/Wallet/RightPanel/DapLastActionsRightPanel.qml"
    ///@detalis Path to the right panel of new payment.
    readonly property string newPaymentMain: "qrc:/screen/" + device + "/Wallet/RightPanel/DapNewPaymentMainRightPanel.qml"
    ///@detalis Path to the right panel of new payment done.
    readonly property string newPaymentDone: "qrc:/screen/" + device + "/Wallet/RightPanel/DapNewPaymentDoneRightPanel.qml"

    property int dapIndexCurrentWallet: -1

    property alias dapWalletRightPanel: stackViewRightPanel
    property alias dapWalletTab:walletTab


    id: walletTab

    dapTopPanel:
        DapWalletTopPanel{
            id: walletTopPanel
            dapComboboxWallet.onCurrentIndexChanged:
            {
                if(dapComboboxWallet.currentIndex != -1)
                {
                    walletScreen.dapListViewWallet.model = dapModelWallets.get(dapComboboxWallet.currentIndex).networks
                    walletScreen.dapNameWalletTitle.text = dapModelWallets.get(dapComboboxWallet.currentIndex).name
                    console.log(dapComboboxWallet.currentIndex)
                    console.log(dapModelWallets.get(dapComboboxWallet.currentIndex))
                    console.log("DapGetWalletHistoryCommand")
                    console.log("   network: " + dapServiceController.CurrentNetwork)
                    console.log("   chain: " + dapServiceController.CurrentChain)
                    console.log("   wallet address: " + dapWallets[walletTopPanel.dapComboboxWallet.currentIndex].findAddress(dapServiceController.CurrentNetwork))
                    dapServiceController.requestToService("DapGetWalletHistoryCommand", dapServiceController.CurrentNetwork, dapServiceController.CurrentChain, dapWallets[walletTopPanel.dapComboboxWallet.currentIndex].findAddress(dapServiceController.CurrentNetwork));
                    walletTab.state = "WALLETSHOW"
                }
            }
            dapAddWalletButton.onClicked:
            {
                createWallet()
                walletScreen.dapWalletCreateFrame.visible = false
            }
    }

    dapScreen:
        DapWalletScreen
        {
            id: walletScreen
            dapAddWalletButton.onClicked:
            {
                createWallet()
                walletScreen.dapWalletCreateFrame.visible = false
            }
            dapButtonNewPayment.onClicked:
            {
                console.log("New payment")
                console.log("wallet from: " + walletTopPanel.dapComboboxWallet.mainLineText)
                console.log("Current network index: "+ dapServiceController.IndexCurrentNetwork);
                console.log("address wallet from: " + dapWallets[walletTopPanel.dapComboboxWallet.currentIndex].findAddress(dapServiceController.CurrentNetwork));
                currentRightPanel = dapRightPanel.push({item:Qt.resolvedUrl(newPaymentMain),
                                                        properties: {
                                                            dapCmboBoxTokenModel: dapModelWallets.get(walletTopPanel.dapComboboxWallet.currentIndex).networks,
                                                            dapCurrentWallet:  walletTopPanel.dapComboboxWallet.mainLineText,
                                                            dapCmboBoxTokenModel: dapModelWallets.get(walletTopPanel.dapComboboxWallet.currentIndex)
                                                               .networks.get(dapServiceController.IndexCurrentNetwork).tokens,
                                                            dapTextSenderWalletAddress: dapWallets[walletTopPanel.dapComboboxWallet.currentIndex]
                                                               .findAddress(dapServiceController.CurrentNetwork)
                                                        }
                                                       });
            }
        }

    dapRightPanel:
        StackView
        {
            id: stackViewRightPanel
            initialItem: Qt.resolvedUrl(lastActionsWallet);
            anchors.fill: parent
            width: 400
            delegate:
                StackViewDelegate
                {
                    pushTransition: StackViewTransition { }
                }
        }

    state: "WALLETDEFAULT"

    states:
    [
        State
        {
            name: "WALLETDEFAULT"
            PropertyChanges
            {
                target: walletScreen.dapWalletCreateFrame;
                visible: true
            }
            PropertyChanges
            {
                target: walletScreen.dapTitleBlock;
                visible: false
            }
            PropertyChanges
            {
                target: walletScreen.dapListViewWallet;
                visible: false
            }
            PropertyChanges
            {
                target: dapRightPanel;
                visible: false
            }
        },
        State
        {
            name: "WALLETSHOW"
            PropertyChanges
            {
                target: walletScreen.dapWalletCreateFrame;
                visible: false
            }
            PropertyChanges
            {
                target: walletScreen.dapTitleBlock;
                visible: true
            }
            PropertyChanges
            {
                target: walletScreen.dapListViewWallet;
                visible: true
            }
            PropertyChanges
            {
                target: dapRightPanel;
                visible: true
            }
        },
        State
        {
            name: "WALLETCREATE"
            PropertyChanges
            {
                target: walletScreen.dapWalletCreateFrame;
                visible: true
            }
            PropertyChanges
            {
                target: walletScreen.dapTitleBlock;
                visible: false
            }
            PropertyChanges
            {
                target: walletScreen.dapListViewWallet;
                visible: false
            }
            PropertyChanges
            {
                target: dapRightPanel;
                visible: true
            }
        }
    ]
    // Signal-slot connection realizing panel switching depending on predefined rules
    Connections
    {
        target: currentRightPanel
        onNextActivated:
        {
            currentRightPanel = dapWalletRightPanel.push(currentRightPanel.dapNextRightPanel);
            if(parametrsRightPanel === lastActionsWallet)
            {
                console.log("DapGetWalletHistoryCommand")
                console.log("   network: " + dapServiceController.CurrentNetwork)
                console.log("   chain: " + dapServiceController.CurrentChain)
                console.log("   wallet address: " + dapWallets[walletTopPanel.dapComboboxWallet.currentIndex].findAddress(dapServiceController.CurrentNetwork))
                dapServiceController.requestToService("DapGetWalletHistoryCommand", dapServiceController.CurrentNetwork, dapServiceController.CurrentChain, dapWallets[walletTopPanel.dapComboboxWallet.currentIndex].findAddress(dapServiceController.CurrentNetwork));
            }
        }
        onPreviousActivated:
        {
            currentRightPanel = dapWalletRightPanel.push(currentRightPanel.dapPreviousRightPanel);
            if(parametrsRightPanel === lastActionsWallet)
            {
                console.log("DapGetWalletHistoryCommand")
                console.log("   network: " + dapServiceController.CurrentNetwork)
                console.log("   chain: " + dapServiceController.CurrentChain)
                console.log("   wallet address: " + dapWallets[walletTopPanel.dapComboboxWallet.currentIndex].findAddress(dapServiceController.CurrentNetwork))
                dapServiceController.requestToService("DapGetWalletHistoryCommand", dapServiceController.CurrentNetwork, dapServiceController.CurrentChain, dapWallets[walletTopPanel.dapComboboxWallet.currentIndex].findAddress(dapServiceController.CurrentNetwork));
            }
        }
    }

    Connections
    {
        target: dapMainWindow
        onModelWalletsUpdated:
        {
            walletTopPanel.dapComboboxWallet.currentIndex = dapIndexCurrentWallet == -1 ? (dapModelWallets.count > 0 ? 0 : -1) : dapIndexCurrentWallet
            walletTopPanel.visible = (dapModelWallets.count > 0)?true:false
        }
    }

    Connections
    {
        target: dapServiceController
        onMempoolProcessed:
        {
            update()
        }
        onWalletCreated:
        {
            update()
        }
    }


    function update()
    {
        dapIndexCurrentWallet = walletTopPanel.dapComboboxWallet.currentIndex
        dapWallets.length = 0
        dapModelWallets.clear()
        dapServiceController.requestToService("DapGetWalletsInfoCommand");
    }

    function createWallet()
    {
        if(state !== "WALLETSHOW")
            state = "WALLETCREATE"
        currentRightPanel = stackViewRightPanel.push({item:Qt.resolvedUrl(inputNameWallet)});
    }
}
