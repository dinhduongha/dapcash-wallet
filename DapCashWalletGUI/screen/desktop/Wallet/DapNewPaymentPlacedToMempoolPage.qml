import QtQuick 2.7
import "../RightPanel"

DapRightPanelPage {
    id: page

    caption: qsTr("New payment")

    Text {
        id: textPlacedToMempool

        y: 178 * pt
        width: page.width
        font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular28 // TODO change size to 28
        elide: Text.ElideRight
        color: "#241E46"
        horizontalAlignment: Qt.AlignHCenter
        text: qsTr("Placed to mempool")
    }
    Text {
        id: textStatus

        y: 242 * pt
        width: page.width
        font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular28 // TODO change size to 22
        elide: Text.ElideRight
        color: "#49436A"
        horizontalAlignment: Qt.AlignHCenter
        text: qsTr("Status")
    }
    Text {
        id: textPending

        y: 275 * pt
        width: page.width
        font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular28 // TODO change size to 28
        elide: Text.ElideRight
        color: "#241E46"
        horizontalAlignment: Qt.AlignHCenter
        text: qsTr("Pending")
    }

    DapRightPanelButton {
        y: 499 * pt
        width: page.width
        button.width: 132 * pt
        button.text: qsTr("Done")
        button.onClicked: app.stateMachine.submitEvent(app.stateMachine.rightPanelClosed)
    }
}
