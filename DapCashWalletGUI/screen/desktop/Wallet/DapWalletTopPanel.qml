import QtQuick 2.4
import QtQuick.Controls 2.0
import Demlabs 1.0
import "../../"
import "qrc:/widgets"

DapTopPanel
{
    property alias dapAddWalletButton: addWalletButton
    property alias dapComboboxWallet: comboboxWallet
    color: "#241E46"

    // Static text "Wallet"
    Label
    {
        id: textHeaderWallet
        text: qsTr("Wallet")
        anchors.left: parent.left
        anchors.leftMargin: 24 * pt
        anchors.verticalCenter: parent.verticalCenter
        font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular14
        color: "#ACAAB5"
    }

    // Wallet selection combo box
    Rectangle
    {
        id: frameComboBoxWallet

        anchors.left: textHeaderWallet.right
        anchors.verticalCenter: parent.verticalCenter
        anchors.leftMargin: 30 * pt
        width: 148 * pt
        color: "transparent"

        DapComboBox
        {
            id: comboboxWallet
            model: dapModelWallets
            comboBoxTextRole: ["name"]
            mainLineText: "all wallets"
            isDefaultNeedToAppend: false
            indicatorImageNormal: "qrc:/resources/icons/ic_arrow_drop_down.png"
            indicatorImageActive: "qrc:/resources/icons/ic_arrow_drop_up.png"
            sidePaddingNormal: 0 * pt
            sidePaddingActive: 16 * pt
            normalColorText: "#241E46"
            hilightColorText: "#FFFFFF"
            normalColorTopText: "#FFFFFF"
            hilightColorTopText: "#241E46"
            hilightColor: "#330F54"
            normalTopColor: "#241E46"
            widthPopupComboBoxNormal: 148 * pt
            widthPopupComboBoxActive: 180 * pt
            heightComboBoxNormal: 24 * pt
            heightComboBoxActive: 44 * pt
            bottomIntervalListElement: 8 * pt
            topEffect: false
            x: popup.visible ? sidePaddingActive * (-1) : sidePaddingNormal
            normalColor: "#FFFFFF"
            hilightTopColor: normalColor
            paddingTopItemDelegate: 8 * pt
            heightListElement: 32 * pt
            intervalListElement: 10 * pt
            indicatorWidth: 24 * pt
            indicatorHeight: indicatorWidth
            indicatorLeftInterval: 8 * pt
            colorTopNormalDropShadow: "#00000000"
            colorDropShadow: "#40ABABAB"
            fontComboBox: [dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular14]
            colorMainTextComboBox: [["#FFFFFF", "#241E46"]]
            colorTextComboBox: [["#241E46", "#FFFFFF"]]


        }
    }

    // Wallet create button
    DapButton
    {
        id: addWalletButton
        textButton: "New wallet"
        anchors.right: parent.right
        anchors.rightMargin: 24 * pt
        anchors.verticalCenter: parent.verticalCenter
        normalImageButton: "qrc:/resources/icons/new-wallet_icon_dark.png"
        hoverImageButton: "qrc:/resources/icons/new-wallet_icon_dark.png"
        implicitHeight: 36 * pt
        implicitWidth: 120 * pt
        widthImageButton: 23 * pt
        heightImageButton: 23 * pt
        indentImageLeftButton: 10 * pt
        colorBackgroundNormal: "#241E46"
        colorBackgroundHover: "#f1352b6a"
        colorButtonTextNormal: "#FFFFFF"
        colorButtonTextHover: "#FFFFFF"
        indentTextRight: 10 * pt
        fontButton: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular14
        borderColorButton: "#000000"
        borderWidthButton: 0
        horizontalAligmentText:Qt.AlignRight
        colorTextButton: "#FFFFFF"

    }

    Component.onCompleted:
    {
        walletTopPanel.visible = (dapModelWallets.count > 0)?true:false
        if(dapModelWallets.count > 0)
        {
            if(dapModelWallets.get(0).name === "all wallets")
            {
                dapComboboxWallet.currentIndex = -1;
                dapModelWallets.remove(0, 1);
                dapComboboxWallet.currentIndex = 0;
            }
        }
    }
}
