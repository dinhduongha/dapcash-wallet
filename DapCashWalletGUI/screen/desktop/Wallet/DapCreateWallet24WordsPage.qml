import QtQuick 2.7
import "../RightPanel"

DapRightPanelPage {
    id: page

    function setWords(words) {
        wordsModel.clear();
        for (var i = 0; i < words.length; ++i) {
            wordsModel.append({ word: words[i] });
        }

    }

    caption: qsTr("New wallet")

    // TODO for test
    Component.onCompleted: {
        var words = [];
        for (var i = 0; i < 24; ++i) {
            words.push("Word " + (i + 1));
        }
        setWords(words);
    }

    ListModel {
        id: wordsModel
    }

    Column {
        DapRightPanelGroupHeader {
            width: page.width
            text: qsTr("Recovery method: 24 words")
            bottomInset: 12 * pt
        }

        Text {
            width: page.width
            leftPadding: 36 * pt
            topPadding: 16 * pt
            rightPadding: 36 * pt
            bottomPadding: 33 * pt

            font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular16
            wrapMode: Text.WordWrap
            horizontalAlignment: Qt.AlignHCenter
            color: "#FF0300"
            text: qsTr("Keep these words in a safe place. They will be required to restore your wallet in case of loss of access to it.")
        }

        Row {
            Repeater {
                model: 2

                Column {
                    spacing: 1 * pt

                    Repeater {
                        id: repeater2

                        property int arrayOffset: wordsModel.count / 2 * index

                        model: 12

                        Text {
                            onBottomPaddingChanged: console.log("bottompadding", bottomPadding)
                            onTopPaddingChanged: console.log("bottompadding", topPadding)

                            width: page.width / 2
                            leftPadding: 0
                            topPadding: 0
                            rightPadding: 0
                            bottomPadding: 0
                            font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular16
                            color: "#241E46"
                            elide: Text.ElideRight
                            horizontalAlignment: Qt.AlignHCenter
                            text: wordsModel.count > (repeater2.arrayOffset + index) ? wordsModel.get(repeater2.arrayOffset + index).word : ""
                        }
                    }
                }
            }
        }

        Text {
            id: textWordsCopiedToClipboard

            width: page.width
            leftPadding: 36 * pt
            topPadding: 16 * pt
            rightPadding: 36 * pt
            bottomPadding: 12 * pt
            opacity: 0.0
            font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular16
            wrapMode: Text.WordWrap
            horizontalAlignment: Qt.AlignHCenter
            color: "#7EB400"
            text: qsTr("Recovery words copied to clipboard. Keep them in a safe place before proceeding to the next step.")
        }

        DapRightPanelTwoButtons {
            width: page.width
            button1.width: 132 * pt
            button2.width: 132 * pt
            button1.text: qsTr("Next");
            button2.text: qsTr("Copy")

            button1.onClicked: {
                page.pushPageRequest(walletCreatedSuccessfully);
            }

            button2.onClicked: {
                var text = "";
                for (var i = 0; i < wordsModel.count; ++i) {
                    if (i > 0)
                        text += " ";
                    text += wordsModel.get(i).word;
                }
                app.setClipboardText(text);
                textWordsCopiedToClipboard.opacity = 1.0;
            }
        }
    }

    Component {
        id: walletCreatedSuccessfully

        DapWalletCreatedSuccessfullyPage {
        }
    }
}
