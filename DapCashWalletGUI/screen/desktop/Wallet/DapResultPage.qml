import QtQuick 2.7
import "../RightPanel"

DapRightPanelPage {
    id: page

    caption: qsTr("New wallet")

    Text {
        id: messageText

        y: 178 * pt
        width: page.width
        leftPadding: 36 * pt
        topPadding: 16 * pt
        rightPadding: 36 * pt
        bottomPadding: 32 * pt

        font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular28 // TODO change size to 28
        wrapMode: Text.WordWrap
        horizontalAlignment: Qt.AlignHCenter
        color: "#241E46"
        text: app.stateMachine.wallet_noWallets_creating ?
                    app.stateMachine.wallet_noWallets_creating_result_success ? qsTr("Wallet created successfully") : qsTr("Error while wallet creating")
                    : qsTr("Creating transaction error!")

    }

    DapRightPanelButton {
        y: 499 * pt
        width: page.width
        button.width: 132 * pt
        button.text: app.stateMachine.wallet_noWallets_creating_result_success ? qsTr("Done") : qsTr("Back")
        button.onClicked: {
            if (app.stateMachine.wallet_noWallets_creating_result_success)
                app.stateMachine.submitEvent(app.stateMachine.rightPanelClosed)
            else
                app.stateMachine.submitEvent(app.stateMachine.rightPanelReverted)
        }
    }

}
