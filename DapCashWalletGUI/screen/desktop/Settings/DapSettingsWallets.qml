import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQml 2.2
import Demlabs 1.0
import "qrc:/widgets"
import "../Controls"


Rectangle{
    id: control
    color: "transparent"
    border.width: 1 * pt
    border.color: "#E2E1E6"
    radius: 8 * pt
    clip: true
    width: 692*pt
    height: textSettings.height + listView.contentHeight <= 552*pt ?
                textSettings.height + listView.contentHeight : 552*pt

    Text {
        id: textSettings
        x: 14 * pt
        height: 36 * pt
        verticalAlignment: Text.AlignVCenter
        font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandBold14
        color: "#3E3853"
        text: qsTr("General settings")
    }

    ListView {
        id: listView
        y: 36 * pt
        width: parent.width
        height: parent.height < 552*pt ? parent.height - y : parent.height - y - control.radius
        clip: true

        model: walletsModel

        header:  Rectangle {
            id: currentWallet
            width: parent.width
            height: 30 * pt
            color: "#49436A"

            Text {
                x: 14 * pt
                anchors.verticalCenter: parent.verticalCenter
                font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular12
                color: "#FFFFFF"
                text: qsTr("Current wallet")
            }
        }

        delegate: Item {
            id: delegateItem
            width: ListView.view.width
            height: 60*pt

            Rectangle {
                visible: index !== 0
                width: parent.width
                height: 1 * pt
                color: "#E2E1E6"
            }

            Item {
                anchors {
                    top: parent.top
                    bottom: parent.bottom
                    left: parent.left
                    leftMargin: 14 * pt
                    right: parent.right
                    rightMargin: 10 * pt
                }
                clip: true

                DapRadioButton{
                    anchors.verticalCenter: parent.verticalCenter
                    checked: app.currentWallet === wallet
                    onClicked: app.currentWallet = wallet
                }

                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    x:40*pt
                    color: "#2A2352"
                    font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular14
                    elide: Text.ElideRight
                    text: name
                }

                Text {
                    id: textAddress
                    anchors.verticalCenter: parent.verticalCenter
                    x:220*pt
                    color: "#2A2352"
                    width:380*pt
                    wrapMode: Text.WrapAnywhere
                    font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular12
                    text: address
                }

                DapButton{
                    id: btnCopyAddress
                    anchors {
                        right: parent.right
                        verticalCenter: parent.verticalCenter
                    }
                    heightButton:20*pt
                    widthButton:20*pt
                    widthImageButton:20*pt
                    heightImageButton:20*pt
                    colorBackgroundNormal:"transparent"
                    colorBackgroundHover: "transparent"
                    normalImageButton: "qrc:/resources/icons/ic_copy-wallet_adress-setting.svg"
                    hoverImageButton: "qrc:/resources/icons/ic_copy-wallet_adress-setting_hover.svg"
                    onClicked: app.setClipboardText(address)
                }
            }
        }

        ScrollBar.vertical: ScrollBar {}
    }
}
