import QtQuick 2.4
import QtQuick.Controls 2.0
import "qrc:/widgets"

Item
{
    id:root

    DapSettingsWallets {
        anchors{
            left: root.left
            leftMargin: 24*pt
            top:root.top
            topMargin: 24*pt
        }

    }
}
