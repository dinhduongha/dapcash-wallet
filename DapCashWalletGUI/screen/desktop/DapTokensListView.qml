import QtQuick 2.0
import QtQuick.Controls 2.2
import Demlabs 1.0

Rectangle
{
    id: root
    border.color: "#E2E1E6"
    border.width: 1 * pt
    radius: 8 * pt
    clip: true

    Text {
        id: headerText

        anchors.top: parent.top
        x: 15 * pt
        height: 34 * pt
        width: parent.width
        text: qsTr("Tokens")
        verticalAlignment: Text.AlignVCenter
        font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandBold14
        color: "#3E3853"
    }


    ListView
    {
        id: tokensListView

        anchors { top: headerText.bottom; left: root.left; right: root.right; bottom: root.bottom}
        model: app.currentWallet ? app.currentWallet.balanceModel : undefined
        clip: true

        ScrollBar.vertical: ScrollBar {
            policy: ScrollBar.AsNeeded
        }

        delegate: Column {
            width: parent.width
            height: networkListView.height + networkHeader.height

            Rectangle {
                id: networkHeader
                height: 30 * pt
                width: parent.width
                color: "#49436A"

                Text {
                    x: 16 * pt
                    anchors.verticalCenter: parent.verticalCenter
                    text: network
                    elide: Text.ElideRight
                    font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular12
                    color: "#FFFFFF"
                }
            }

            ListView {
                id: networkListView

                height: contentHeight
                width: parent.width
                model: balanceModel

                delegate: Column {
                    id: delegate

                    property DapTokenValue tokenValue: model.tokenValue

                    height: line.height + listItem.height;
                    width: parent.width

                    Rectangle {
                        id: line

                        height: (index > 0) ? 1 * pt : 0;
                        width: delegate.width
                        color: "#E3E2E6"
                        visible: (index > 0)
                    }

                    Item {
                        id: listItem

                        height: 50 * pt
                        width: parent.width

                        Text {
                            id: tokenText

                            font:  dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular16
                            color: '#070023'
                            anchors { left: parent.left; right: valueText.left; verticalCenter: parent.verticalCenter }
                            anchors.leftMargin: 13 * pt
                            horizontalAlignment: Text.AlignLeft
                            elide: Text.ElideRight
                            text: tokenValue.token.name
                        }

                        Text {
                            id: valueText

                            font:  dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular14
                            color: '#070023'
                            anchors { right: parent.right; verticalCenter: parent.verticalCenter }
                            anchors.rightMargin: 13 * pt
                            horizontalAlignment: Text.AlignRight
                            text: tokenValue.representation
                        }
                    }

                }
            }
        }
    }
}
