import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQml 2.2

Rectangle {
    id: control

    color: "transparent"
    border.width: 1 * pt
    border.color: "#E2E1E6"
    radius: 8 * pt
    clip: true

    Text {
        id: textTransactions
        x: 14 * pt
        height: 36 * pt
        color: "#2A2352"
        verticalAlignment: Qt.AlignVCenter
        font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandBold14
        text: qsTr("Transactions")
    }

    ListView {
        id: listView
        y: 36 * pt
        width: parent.width
        height: parent.height - y - control.radius
        clip: true

        model: transactionsModel

        section.property: "date"
        section.criteria: ViewSection.FullString
        section.delegate: Rectangle {
            id: dateSection
            width: parent.width
            height: 30 * pt
            color: "#49436A"

            Text {
                x: 14 * pt
                anchors.verticalCenter: parent.verticalCenter
                font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular12
                color: "#FFFFFF"
                text: section
            }
        }

        delegate: Item {
            id: delegateItem

            property DapTransaction transaction: model.display

            width: ListView.view.width
            height: 50*pt

            Rectangle {
                width: parent.width
                height: 1 * pt
                color: "#E2E1E6"
            }

            Item {
                anchors {
                    top: parent.top
                    bottom: parent.bottom

                    left: parent.left
                    leftMargin: 14 * pt
                    right: parent.right
                    rightMargin: 30 * pt
                }

                Text {
                    y: 9 * pt
                    color: "#757184"
                    font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular11
                    text: qsTr("Network name")
                }

                Text {
                    y: 25 * pt
                    color: "#2A2352"
                    font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular12
                    elide: Text.ElideRight
                    text: display.network.name
                }

                Text {
                    id: textStatus
                    x: 255 * pt
                    anchors.verticalCenter: parent.verticalCenter
                    width: textAmount.x - x
                    color: "#2A2352"
                    font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular12
                    elide: Text.ElideRight
                    text: display.statusRepresentation()
                }

                Text {
                    id: textAmount
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    color: "#2A2352"
                    font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandRegular14
                    elide: Text.ElideRight
                    text: transaction.sumRepresentation
                }
            }
        }

        footer: Rectangle {
            width: parent.width
            height: 1 * pt
            color: "#E2E1E6"
            visible: listView.count > 0
        }

        ScrollBar.vertical: ScrollBar {}
    }
}
