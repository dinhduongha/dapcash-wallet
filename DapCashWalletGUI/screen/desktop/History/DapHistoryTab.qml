import QtQuick 2.7
import "qrc:/widgets"
import "../RightPanel"

Item {
    id: tab

    Column {
        anchors.fill: parent

        DapTopPanel {
            id: topPanel
            color: "#241E46"

            source: "qrc:/screen/desktop/History/DapHistoryTopPanel.qml"
        }


        Item {
            height: parent.height - topPanel.height
            width: parent.width


            DapHistoryTransactions {
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: rightPanel.left
                anchors.bottom: parent.bottom

                anchors.leftMargin: 24*pt
                anchors.topMargin: 24*pt
                anchors.bottomMargin: 24*pt
            }

            DapHistoryRightPanel {
                id: rightPanel

                width: 398 * pt
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.right: parent.right
            }
        }
    }
}
