import QtQuick 2.0
import QtQuick.Controls 2.2
import "../RightPanel"
import Demlabs 1.0


Item {
    id: root

    Item{
        anchors{
            left: parent.left
            leftMargin: 39*pt
            top:parent.top
            topMargin: 37*pt
        }

        Text {
            id: status

            height: 16*pt
            text: qsTr("Status")
            font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandBold14
            color: "#2A2352"
        }

        Column {
            id: statusCheckBoxColumn

            anchors{ top: status.bottom; topMargin: 25*pt }
            spacing: 33*pt

            Repeater {
                model: ListModel {
                    id: statusModel

                    ListElement { text: qsTr("Local");      status: DapTransaction.LOCAL     }
                    ListElement { text: qsTr("Mempool");    status: DapTransaction.MEMPOOL   }
                    ListElement { text: qsTr("Successful"); status: DapTransaction.SUCCESSFUL}
                    ListElement { text: qsTr("Canceled");   status: DapTransaction.CANCELED  }
                }

                DapRightPanelCheckBox {
                    leftInset: 0*pt
                    height: 20*pt
                    checkBox {
                        text: text
                        spacing: 18*pt
                        checked: true
                        onClicked: checkBox.checked === true ? transactionsModel.addStatusFilter(status)
                                                             : transactionsModel.removeStatusFilter(status)
                    }
                }
            }
        }

        Text {
            id: period

            height: 16*pt
            anchors { top: statusCheckBoxColumn.bottom; topMargin: 37*pt }
            text: qsTr("Period")
            font: dapQuicksandFonts.dapMainFontTheme.dapFontQuicksandBold14
            color: "#2A2352"
        }

        DapRightPanelComboBox {
            id: periodCombobox

            anchors { top: period.bottom; topMargin: 15*pt }
            implicitWidth: 320*pt
            comboBox {
                widthPopupComboBoxNormal:320*pt
                model: ListModel {
                    id: dateModel
                    ListElement { text: qsTr("All time");  date: DapTransactionsProxyModel.AllTime   }
                    ListElement { text: qsTr("Today");     date: DapTransactionsProxyModel.Today     }
                    ListElement { text: qsTr("Yesterday"); date: DapTransactionsProxyModel.Yesterday }
                    ListElement { text: qsTr("This week"); date: DapTransactionsProxyModel.ThisWeek  }
                    ListElement { text: qsTr("Last week"); date: DapTransactionsProxyModel.LastWeek  }
                }
                onActivated: transactionsModel.dateFilter = dateModel.get(index).date
            }
        }
    }
}

