; ***************************************************************
; * Authors:
; * Konstantin Papizh <papizh.konstantin@demlabs.net>
; * DeM Labs Inc.   https://demlabs.net
; * DapCash Project https://gitlab.demlabs.net/dapcash
; * Copyright  (c) 2020
; * All rights reserved.
; ***************************************************************

!define MULTIUSER_EXECUTIONLEVEL Admin
;!include "MultiUser.nsh"
!include "MUI2.nsh"
!include "x64.nsh"
!include "modifyConfig.nsh"

!define MUI_ICON		"icon.ico"
!define MUI_UNICON		"icon.ico"

!define APP_NAME		"DapCashWallet"
!define NODE_NAME		"dapcash-node"
!define EXE_NAME		"${APP_NAME}.exe"
!define APP_VERSION		"2.0.5.0"
!define PUBLISHER		"DeMLabs Inc."

!define UNINSTALL_PATH "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}"

!define MUI_COMPONENTSPAGE_TEXT_TOP ""

Unicode true

Name 	"${APP_NAME} ${APP_VERSION}"
OutFile	"${APP_NAME} ${APP_VERSION}.exe"
BrandingText "${APP_NAME} by ${PUBLISHER}"

!define MUI_FINISHPAGE_NOAUTOCLOSE

Var CommonDocuments
Var ConfigPath

VIProductVersion "${APP_VERSION}"
VIAddVersionKey "ProductName"		"${APP_NAME}"
VIAddVersionKey "CompanyName"		"${PUBLISHER}"
VIAddVersionKey "LegalCopyright"	"${PUBLISHER} © 2020"
VIAddVersionKey "FileDescription"	"DapCash Wallet Application"
VIAddVersionKey "FileVersion"		"${APP_VERSION}"

Function .onInit
	${If} ${RunningX64}
		${EnableX64FSRedirection}
		SetRegView 64
	${else}
        MessageBox MB_OK "${APP_NAME} supports x64 architectures only"
        Abort
    ${EndIf}
	ReadRegStr $CommonDocuments HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders" "Common Documents"
	StrCpy $ConfigPath "$CommonDocuments\${NODE_NAME}\etc"
FunctionEnd

AutoCloseWindow true

;Function .onInstSuccess
	
;FunctionEnd

Function UninstallPrevious
	ReadRegStr $R0 HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}" "UninstallString"
	StrCmp $R0 "" done uninst
uninst:
	ClearErrors
	ExecWait '$R0 _?=$INSTDIR' ;Do not copy the uninstaller to a temp file
done:
FunctionEnd

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES  

!insertmacro MUI_LANGUAGE 	"English"
!insertmacro MUI_LANGUAGE 	"Russian"

!macro varPaths
	IfFileExists "$CommonDocuments\${NODE_NAME}\var\log" yesLog 0
	CreateDirectory "$CommonDocuments\${NODE_NAME}\var\log"
yesLog:
	IfFileExists "$CommonDocuments\${NODE_NAME}\var\lib\global_db" yesDb 0
	CreateDirectory "$CommonDocuments\${NODE_NAME}\var\lib\global_db"
yesDb:
	IfFileExists "$CommonDocuments\${NODE_NAME}\var\lib\wallet" yesWallet 0
	CreateDirectory "$CommonDocuments\${NODE_NAME}\var\lib\wallet"	
yesWallet:
	IfFileExists "$CommonDocuments\${NODE_NAME}\var\lib\ca" yesCa 0
	CreateDirectory "$CommonDocuments\${NODE_NAME}\var\lib\ca"
yesCa:
	IfFileExists "$CommonDocuments\${APP_NAME}\log" yesDashLog 0
	CreateDirectory "$CommonDocuments\${APP_NAME}\log"
yesDashLog:
	IfFileExists "$CommonDocuments\${APP_NAME}\data" yesDashData 0
	CreateDirectory "$CommonDocuments\${APP_NAME}\data"
yesDashData:
!macroend

!macro copyDist
	SetOutPath "$CommonDocuments\${NODE_NAME}\etc\network"
	File /r "dist\share\configs\network\"
	File /r "dist\etc\network\"
	SetOutPath "$CommonDocuments\${NODE_NAME}\share" 
	File /r "dist\share\"
	File "/oname=$CommonDocuments\${NODE_NAME}\etc\dapcash-node.cfg" "dist\share\configs\dapcash-node.cfg.tpl"
	File "/oname=$CommonDocuments\${NODE_NAME}\etc\network\dapcash-testnet.cfg" "dist\share\configs\network\dapcash-testnet.cfg.tpl"
!macroend

!insertmacro AdvReplace

!macro killAll
	nsExec::Exec 'taskkill /f /im ${EXE_NAME}'
	nsExec::Exec 'sc stop ${APP_NAME}Service'
	nsExec::Exec 'sc delete ${APP_NAME}Service'
	nsExec::Exec 'taskkill /f /im ${NODE_NAME}.exe'
!macroend

InstallDir "$PROGRAMFILES64\${APP_NAME}"

Section "" uninstallPrev
	Call UninstallPrevious
SectionEnd

Section "${APP_NAME}" CORE
	SectionIn RO			
	SetOutPath "$INSTDIR"
	File "${APP_NAME}.exe"
	File "${APP_NAME}Service.exe"
	File "${NODE_NAME}.exe"
	File "${NODE_NAME}-cli.exe"
	File "${NODE_NAME}-tool.exe"
!insertmacro varPaths
!insertmacro copyDist
!insertmacro modifyConfigFiles
	WriteRegStr HKLM "${UNINSTALL_PATH}" "DisplayName" "${APP_NAME} ${APP_VERSION}"
	WriteRegStr HKLM "${UNINSTALL_PATH}" "UninstallString" "$INSTDIR\Uninstall.exe"
	WriteRegStr HKLM "${UNINSTALL_PATH}" "DisplayVersion" "${APP_VERSION}"
	WriteRegStr HKLM "${UNINSTALL_PATH}" "Publisher" "${PUBLISHER}"
	WriteRegStr HKLM "${UNINSTALL_PATH}" "DisplayIcon" "$INSTDIR\${EXE_NAME}"
	WriteUninstaller "$INSTDIR\Uninstall.exe"
	CreateShortCut "$DESKTOP\${APP_NAME}.lnk" "$INSTDIR\${EXE_NAME}"
SectionEnd

Section "-startApps"
	nsExec::Exec 'dism /online /enable-feature /featurename:MSMQ-Container /featurename:MSMQ-Server /featurename:MSMQ-Multicast'
	nsExec::Exec '"$INSTDIR\${APP_NAME}Service.exe" install'
	nsExec::Exec 'sc failure ${APP_NAME}Service reset= 3600 actions= restart/1000/restart/1000/restart/1000'
	nsExec::Exec 'sc start ${APP_NAME}Service'
	ExecShell "" "$INSTDIR\${NODE_NAME}.exe"
SectionEnd

Section "Uninstall"
	SetRegView 64
	!insertmacro killAll
	Delete "$INSTDIR\${APP_NAME}.exe"
	Delete "$INSTDIR\${APP_NAME}Service.exe"
	Delete "$INSTDIR\${NODE_NAME}.exe"
	Delete "$INSTDIR\${NODE_NAME}-tool.exe"
	Delete "$INSTDIR\${NODE_NAME}-cli.exe"
	DeleteRegKey HKLM "${UNINSTALL_PATH}"
	Delete "$INSTDIR\Uninstall.exe"
	Delete "$DESKTOP\${APP_NAME}.lnk"
	Delete "$DESKTOP\${APP_NAME}Service.lnk"
	RMDir "$INSTDIR"
SectionEnd